﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompileTester
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Console.WriteLine("Compilation has been successful.");
            Console.WriteLine("Press any key to close this window.");
            Console.ReadKey();
            Console.WriteLine("\nBye");
            System.Threading.Thread.Sleep(500);
        }
    }
}
